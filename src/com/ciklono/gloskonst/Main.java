package com.ciklono.gloskonst;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Main {
	
	public static Map<String, String> ESPDIC = new HashMap<>();
	public static Set<String> WORD_BANK = new HashSet<>();
	
	private List<File> chapterFileList = new ArrayList<>();
	private Set<String> bookWordList = new HashSet<>();
	File glossaryDir;
			
	public Main(String chaptersDirPath) {
		File chapterDirectory = new File(chaptersDirPath);
		createGlossaryDir(chaptersDirPath);
		File[] chapterFiles = chapterDirectory.listFiles();
		
	    for (File chapterFile : chapterFiles) {
	    	// Ignore any subdirectories...
	    	if (chapterFile.exists()) {
	    		if (chapterFile.isFile()) {
	    			chapterFileList.add(chapterFile);
	    		} 
	    	}
	      }
	    readDictionary();
	    readWordBank();
	    writeWordBank();
	}
	
	private void readDictionary() {
		
		try (InputStream is = Main.class.getResourceAsStream("espdic.txt");
				InputStreamReader ir = new InputStreamReader(is, StandardCharsets.UTF_8);
				BufferedReader br = new BufferedReader(ir)) 
		{
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				String[] wordDef = sCurrentLine.split(" : ");
				if (wordDef.length == 2) {
					ESPDIC.put(wordDef[0], wordDef[1]);
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(-1);
		} 
	}
	
	private void readWordBank() {
		try (InputStream is = Main.class.getResourceAsStream("word_bank.txt");
				InputStreamReader ir = new InputStreamReader(is, StandardCharsets.UTF_8);
				BufferedReader br = new BufferedReader(ir)) 
		{
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				WORD_BANK.add(sCurrentLine);
			}
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(-1);
		} 
	}
	
	private void writeWordBank() {
		URL url = Main.class.getResource("word_bank.txt");
		File wordBankFile;
		try {
			wordBankFile = new File(url.toURI().getPath());
		} catch(Throwable e) {
			e.printStackTrace();
			System.out.println("Continuing without alphabetizing and removing duplicates from word bank.");
			return;
		}
		// Alphabetize word bank and write back to the file without duplicates
		List<String> words = new ArrayList<>(WORD_BANK);
		Collections.sort(words);
		try (
				FileWriter fw = new FileWriter(wordBankFile);
				BufferedWriter bw = new BufferedWriter(fw);) 
		{
			for (String word : words) {
				bw.write(word);
				bw.newLine();
				bw.flush();
			}
			bw.close();
			fw.close();
		} catch (Throwable e) {
			e.printStackTrace();
		} 	
	}
	
	void createGlossaryDir(String chaptersDirPath) {
		glossaryDir = new File(chaptersDirPath + "\\" + "glossaries");
		if (!glossaryDir.isDirectory()) {
		  boolean success = glossaryDir.mkdirs();
		  if (success) {
		    System.out.println("Created path: " + glossaryDir.getPath());
		  } else {
		    System.out.println("Could not create path: " + glossaryDir.getPath());
		  }
		} else {
		  System.out.println("Path exists: " + glossaryDir.getPath());
		}
	}
	
	private void execute() {
		for (File chapterFile : chapterFileList) {
			ChapterReader cr = new ChapterReader(chapterFile, glossaryDir);
			cr.createGlossary(bookWordList);
		}
	}

	// Arg example: C:\Users\Owner\Documents\Esperanto\Books\Alice_In_Wonderland\chapters
	public static void main(String[] args) {
		if (args != null && args.length > 0) {
			String chapterListFilePath = args[0];
			Main main = new Main(chapterListFilePath);
			main.execute();
		} else {
			System.out.println("Error: requires the path of the chapter directory as an argument." );
		}
	}

}
