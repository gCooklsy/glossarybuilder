package com.ciklono.gloskonst;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChapterReader {
	
	public static String EO_CHARS = "[abcdefghijklmnoprstuvzABCDEFGHIJKLMNOPRSTUVZ\\-ŝŜĉĈĝĜĥĤŭŬĵĴ]+";
	public static String EO_WORD = "^[abcdefghijklmnoprstuvzABCDEFGHIJKLMNOPRSTUVZŝŜĉĈĝĜĥĤŭŬĵĴ]+$";
	public static Pattern EO_SUBSTRING_PATTERN = Pattern.compile(EO_CHARS);
	
	private String chapterName;
	private File glossaryDir;
	private Set<String> wordsInChapter = new HashSet<>();

	ChapterReader(File chapterFile, File glossaryDir) {
		this.chapterName = chapterFile.getName();
		this.glossaryDir = glossaryDir;
		try (FileReader fr = new FileReader(chapterFile);
				BufferedReader br = new BufferedReader(fr);) 
			{
				String sCurrentLine;
				while ((sCurrentLine = br.readLine()) != null) {
					String[] words = sCurrentLine.split("\\s");
					for (String word : words) {
						word = cleanse(word);
						word = removeHyphens(word);
						word = removeEndings(word);
						if (word.length() > 1) {
							if (isEsperanto(word)) {
								wordsInChapter.add(word);
							}
						}
					}
					
				}			
			} catch (Throwable e) {
				e.printStackTrace();
				System.exit(-1);
			} 
	}
	
	private boolean isEsperanto(String word) {
		return word.matches(EO_WORD);
	}
	
	private String removeHyphens(String word) {
		return word.replaceAll("\\-", "");
	}
	
	private String removeEndings(String word) {
		// return problem words:
		if (word.equals("kaj") || word.equals("kelkaj")) {
			return word;
		}
		if (word.endsWith("o") || word.endsWith("i") || word.endsWith("a") || word.endsWith("e") || word.endsWith("aŭ")) {
			return word;
		}
		// Nouns, pronouns, adjactives and participles: Drop -jn -n or -j endings
		if (word.endsWith("j") || word.endsWith("n") || word.endsWith("n")) {
			return word.replaceAll("jn$|n$|j$|", "");
		}
		// Verbs: Replace -as -is -os -us -u with -i
		if (word.endsWith("as") || word.endsWith("is") || word.endsWith("os") || word.endsWith("us") || word.endsWith("u")) {
			return word.replaceAll("as$|is$|os$|us$|u$", "i");
		}
		// Otherwise probably a preposition or name
		return word;
	}
	
	private String cleanse(String mixedCaseWord) {
		String result = "";
		String lowerCaseWord = mixedCaseWord.toLowerCase();
		Matcher matcher = EO_SUBSTRING_PATTERN.matcher(lowerCaseWord);
		if (matcher.find())
		{
		    result = matcher.group(0);
		}
		return result;
	}
	
	void createGlossary(Set<String> bookWordList) {
		List<String> chapterGlossary = new ArrayList<>();
		for (String word : wordsInChapter) {
			if ((!Main.WORD_BANK.contains(word)) && (!bookWordList.contains(word))) {
				bookWordList.add(word);
				String def = lookupDef(word);
				if (!def.isEmpty()) {
					chapterGlossary.add(word + " - " + def);
				}
			}
		}
		Collections.sort(chapterGlossary);
		File glossaryFile = new File(glossaryDir, chapterName);
		try (FileWriter fw = new FileWriter(glossaryFile);
				BufferedWriter bw = new BufferedWriter(fw);) 
		{
			bw.write("Definitions from ESPDIC (Esperanto – English Dictionary) – 2 August 2015 - Paul Denisowski (www.denisowski.org)");
			bw.newLine();
			bw.flush();
			for (String wordDef : chapterGlossary) {
				bw.write(wordDef);
				bw.newLine();
				bw.flush();
			}
			bw.close();
			fw.close();
		} catch (Throwable e) {
			e.printStackTrace();
		} 
	}
	
	String lookupDef(String word) {
		String def = Main.ESPDIC.get(word);
		if (def == null) {
			def = "";
		}
		def = def.trim();
		return def;
	}
}
