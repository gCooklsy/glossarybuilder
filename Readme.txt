This is a Esperanto-English glossary builder to be used on the text files containing the chapters of an Esperanto book. It will create
a "glossaries" directory containing a glossary for each chapter of the book. Each glossary file will contain definitions of all words 
in the chapter which:
1. were not found in the word_bank.txt file (the user's current vocabulary)
2. were not found not in a previous chapter of the book
3. are found in the ESPDIC dictionary

It was created as an Eclipse project and is probably best run from within Eclipse.

It contains the espdic.txt Esperanto-English dictionary, and a word_bank.txt file to store the user's existing vocabulary. 


-------------------------------------------------
Word Bank File:

The word_bank.txt contains one word per line. Each time the program is run, duplicates will be removed and the file will be re-written
in alphabetically order (with ĉapelitaj literoj at the end). Each user would need to prepare his/her own word_bank.txt file 
since it contains words the user already knows. 

If the app is to be run as a jar, the word_bank.txt would need to be moved to a path outside the jar.

The file comes with the "Gerda Malaperis" vocabulary and a few other basic words. It currently must be manually maintained. :-(


-------------------------------------------------
Data Preparation:

The book should be in a text format.
Copy each chapter of the book into a text file of its own in a separate chapters directory. The chapter files should be named 
so that they are in the order you would read them, e.g.: 00_Intro.txt, 01_Chapter1.text, ..., 38_Chapter38.text, 39_Epilogue.txt


-------------------------------------------------
Running Instructions:

In your run configuration (run-time arguments): You'll need to add the absolute path to the directory containing the chapter files, e.g.:

C:\Users\Owner\Documents\Esperanto\Books\Doktoro_Jekyll_kaj_Sinjoro_Hyde\chapters

You also must add the following VM argument to make it use UTF-8 everywhere:

-Dfile.encoding=UTF8


-------------------------------------------------
ESPDIC File:

ESPDIC (Esperanto English Dictionary) by Paul Denisowski is licensed under a Creative Commons Attribution 3.0 Unported License.
It may be used, transmited, or modified for any purpose, including commercial purposes, as long as the source is properly attributed. 
See: http://www.denisowski.org/Esperanto/ESPDIC/espdic_readme.htm 


-------------------------------------------------
License and Copyright

This application is licensed under a Creative Commons Attribution 3.0 Unported License. It may be used, transmited, or modified for 
any purpose, including commercial purposes, as long as the source is properly attributed.
Source code is publicly hosted at: https://bitbucket.org/gCooklsy/glossarybuilder

© Gregory A. Smith - April 6, 2016